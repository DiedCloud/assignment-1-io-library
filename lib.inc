section .data
 %define STDOUT 1
 %define SYSCALL_WRITE 1
 %define SYSCALL_EXIT 60
 %define STDIN 0
 %define SYSCALL_READ 0
 %define ONE_CHAR_STRING_LENGTH 1
 %define MAX_UINT_STR_LEN 21        ; 2^64-1 contains 20 char-s

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rdi, rdi
    mov rax, SYSCALL_EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    cmp byte [rdi+rax], 0
    je .break
    inc rax
    jmp .loop
    .break:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi             ; string addres
    mov rdx, rax        ; string length
    mov rax, SYSCALL_WRITE 
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp                    ; string addres
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    mov rdx, ONE_CHAR_STRING_LENGTH ; string length
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    call print_char
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge print_uint

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi

    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp
    sub rsp, MAX_UINT_STR_LEN
    dec r8
    mov [r8], byte 0            ; null-terminator
    
    mov rax, rdi                ; number, that will be divided
    mov r10, 10

    .loop:
    xor rdx, rdx
    div r10
    add dl, '0'                 ; number ascii offset
    dec r8
    mov [r8], dl                ; store string to stack
    test rax, rax
    jne .loop

    mov rdi, r8                 ; printing string from stack
    call print_string

    add rsp, MAX_UINT_STR_LEN   ; restore old rsp
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
    mov ah, [rsi]
    mov al, [rdi]
    cmp ah, al
    jne .bad_ending

    test ah, ah
    je .good_ending
    inc rdi
    inc rsi
    jmp .loop
    
    .bad_ending:
    xor rax, rax
    ret
    .good_ending:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp

    mov rax, SYSCALL_READ
    mov rdi, STDIN
    mov rsi, rsp        ; string address
    mov rdx, 1          ; string length
    syscall

    test rax, rax
    je .end

    xor rax, rax
    mov al, [rsp]

    .end:
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi            ; start address
    push rdi            ; index
    add rdi, rsi
    push rdi            ; end address

    test rsi, rsi
    je .buffer_overflow

    .skip_loop:
    call read_char
    test rax, rax
    je .buffer_overflow
    cmp al, ` `
    je .skip_loop
    cmp al, `\t`
    je .skip_loop
    cmp al, `\n`
    je .skip_loop

    .read_loop:
    mov rdi, [rsp+8]    ; [rsp+8] - current index
    mov [rdi], al       ; al was read in skip_loop. Put in by current index
    inc rdi             ; next byte
    cmp rdi, [rsp]      ; if index == end address => buffer_overflow
    je .buffer_overflow
    mov [rsp+8], rdi    ; updete index
    call read_char
    
    test al, al
    je .end_read
    cmp al, ` `
    je .end_read
    cmp al, `\t`
    je .end_read
    cmp al, `\n`
    je .end_read
    jmp .read_loop

    .end_read:
    mov rdi, [rsp+8]    ; 
    mov [rdi], byte 0   ; + null terminator
    mov rax, [rsp+16]   ; rax - start address
    mov rdx, [rsp+8]    ;
    sub rdx, rax        ; rdx - string length
    add rsp, 24         ; clean stack
    ret

    .buffer_overflow:
    mov rax, 0
    mov rdx, 0
    add rsp, 24         ; clean stack
    ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8

    .loop:
    mov r8b, byte[rdi+rdx]

    cmp r8b, '0'
    jl .done
    cmp r8b, '9'
    jg .done

    sub r8b, '0'
    imul rax, 10
    add rax, r8
    inc rdx
    jmp .loop

    .done:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp [rdi], byte '-'
    jne parse_uint      ; if first symbol != '0' => jump to pars_uint
    
    inc rdi
    call parse_uint
    neg rax
    add rdx, 1
    ret

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    add rdx, rsi        ; rdx now is byte after last byte in buffer
    
    .copy_loop:
    cmp rsi, rdx
    je .buffer_overflow
    
    mov al, [rdi]
    mov [rsi], al

    inc rdi
    inc rsi
    test al, al           ; continue if not null
    jne .copy_loop

    sub rsi, rdx
    mov rax, rsi
    ret

    .buffer_overflow:
    xor rax, rax
    ret
